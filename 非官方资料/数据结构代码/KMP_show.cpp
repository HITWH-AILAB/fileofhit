#include<bits/stdc++.h>
using namespace std;

const int N = 1e5+17, M = 1e3+17;

char a[N], b[M];
int lena, lenb, nxt[M];

void print(int bottom, int top, int j){
	for(int p=1;p<=lenb;++p){
		if(p<=top && p >= bottom)
			printf("%c", b[p]+'A'-'a');
		else
			printf("%c", b[p]);
	}
	printf("\n");
	for(int p=1;p<=lenb;++p){
		if(p <= top && p >= bottom)
			printf("+");
		else if(p == j)
			printf("|");
		else
			printf(" ");
	}
	printf("\n");
}

int main(){
	
	//scanf("%s", a+1);
	printf("请输入模式串\n");
	scanf("%s", b+1);
	float t;
	printf("请输入显示时间，单位为s\n");
	scanf("%f", &t);
	
	lena = strlen(a+1);
	lenb = strlen(b+1);
	
	nxt[1] = 0;
	for(int k=0,j=1;j<lenb;){
		_sleep(int(t*1000));
		system("cls");
		
		printf("k=%d, j=%d, mean b[1 ... %d] = b[%d ... %d]?\n\n\n", k, j, k, j-k+1, j);
		
		if(k==0 or b[j] == b[k]){
			
			print(0, 0, j);
			
			printf("Success!\n");
			print(1, k, 0);
			printf("Successful MATCH\n");
			print(j-k+1, j, 0);
			printf("\n");
			
			nxt[++j] = ++k;
			printf("next[%d] = %d\n", j, k);
			
		}else{
			
			
			print(1, k, j);
			printf("Back!\n\n\n\n\n\n\n");
			printf("Try next[%d] = %d , But fail!\n", j, k);
			
			k = nxt[k];
		}
		
		printf("index   ");
		for(int i=1;i<=lenb;++i)
			printf("%4d", i);
		printf("\nnext[i] ");
		for(int i=1;i<=lenb;++i)
			printf("%4d", nxt[i]);		 
		
	}
	_sleep(int(t*1000));
	system("cls");
	
	printf("index   ");
	for(int i=1;i<=lenb;++i)
		printf("%4d", i);
	printf("\nstring  ");
	for(int i=1;i<=lenb;++i)
		printf("   %c", b[i]);
	printf("\nnext[i] ");
	for(int i=1;i<=lenb;++i)
		printf("%4d", nxt[i]);
	
	
	return 0;
}
