wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/MD5SUMS-for-bz2                    
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/MD5SUMS-for-gz                       
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/bug-buddy-2.10.0.tar.bz2              
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/bug-buddy-2.10.0.tar.gz             
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/control-center-2.10.2.tar.bz2      
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/control-center-2.10.2.tar.gz       
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/dasher-3.2.15.tar.bz2               
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/dasher-3.2.15.tar.gz              
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/eel-2.10.1.tar.bz2                  
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/eel-2.10.1.tar.gz                 
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/eog-2.10.2.tar.bz2                
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/eog-2.10.2.tar.gz                  
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/epiphany-1.6.4.tar.bz2              
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/epiphany-1.6.4.tar.gz              
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/evolution-2.2.3.tar.bz2            
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/evolution-2.2.3.tar.gz               
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/evolution-data-server-1.2.3.tar.bz2
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/evolution-data-server-1.2.3.tar.gz  
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/evolution-webcal-2.2.1.tar.bz2     
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/evolution-webcal-2.2.1.tar.gz       
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/file-roller-2.10.4.tar.bz2         
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/file-roller-2.10.4.tar.gz          
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gal-2.4.3.tar.bz2                  
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gal-2.4.3.tar.gz                   
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gcalctool-5.5.42.tar.bz2           
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gcalctool-5.5.42.tar.gz             
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gconf-editor-2.10.0.tar.bz2       
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gconf-editor-2.10.0.tar.gz        
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gdm-2.8.0.1.tar.bz2               
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gdm-2.8.0.1.tar.gz                 
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gedit-2.10.3.tar.bz2              
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gedit-2.10.3.tar.gz               
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/ggv-2.8.5.tar.bz2                 
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/ggv-2.8.5.tar.gz                   
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-applets-2.10.1.tar.bz2       
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-applets-2.10.1.tar.gz       
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-backgrounds-2.10.1.tar.bz2   
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-backgrounds-2.10.1.tar.gz   
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-desktop-2.10.2.tar.bz2       
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-desktop-2.10.2.tar.gz       
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-doc-utils-0.2.0.tar.bz2      
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-doc-utils-0.2.0.tar.gz         
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-games-2.10.1.tar.bz2         
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-games-2.10.1.tar.gz          
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-icon-theme-2.10.1.tar.bz2     
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-icon-theme-2.10.1.tar.gz     
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-keyring-0.4.3.tar.bz2        
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-keyring-0.4.3.tar.gz         
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-mag-0.12.1.tar.bz2           
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-mag-0.12.1.tar.gz            
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-media-2.10.2.tar.bz2         
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-media-2.10.2.tar.gz          
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-menus-2.10.2.tar.bz2         
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-menus-2.10.2.tar.gz          
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-netstatus-2.10.0.tar.bz2    
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-netstatus-2.10.0.tar.gz      
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-nettool-1.2.0.tar.bz2        
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-nettool-1.2.0.tar.gz        
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-panel-2.10.2.tar.bz2       
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-panel-2.10.2.tar.gz            
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-session-2.10.0.tar.bz2        
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-session-2.10.0.tar.gz        
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-speech-0.3.7.tar.bz2        
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-speech-0.3.7.tar.gz           
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-system-monitor-2.10.1.tar.bz2 
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-system-monitor-2.10.1.tar.gz 
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-system-tools-1.2.0.tar.bz2   
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-system-tools-1.2.0.tar.gz    
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-terminal-2.10.0.tar.bz2      
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-terminal-2.10.0.tar.gz       
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-themes-2.10.2.tar.bz2      
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-themes-2.10.2.tar.gz        
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-utils-2.10.1.tar.bz2       
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-utils-2.10.1.tar.gz          
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-volume-manager-1.2.2.tar.bz2  
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome-volume-manager-1.2.2.tar.gz  
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome2-user-docs-2.8.1.tar.bz2     
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnome2-user-docs-2.8.1.tar.gz      
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnomemeeting-1.2.1.tar.bz2        
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnomemeeting-1.2.1.tar.gz         
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnopernicus-0.10.9.tar.bz2        
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gnopernicus-0.10.9.tar.gz          
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gok-1.0.5.tar.bz2                
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gok-1.0.5.tar.gz                  
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gpdf-2.10.0.tar.bz2              
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gpdf-2.10.0.tar.gz               
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gst-plugins-0.8.10.tar.bz2       
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gst-plugins-0.8.10.tar.gz        
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gstreamer-0.8.10.tar.bz2        
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gstreamer-0.8.10.tar.gz         
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gtk-engines-2.6.3.tar.bz2        
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gtk-engines-2.6.3.tar.gz        
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gtkhtml-3.5.7.tar.bz2            
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gtkhtml-3.5.7.tar.gz            
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gtksourceview-1.2.0.tar.bz2      
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gtksourceview-1.2.0.tar.gz       
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gucharmap-1.4.3.tar.bz2          
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/gucharmap-1.4.3.tar.gz            
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/libgail-gnome-1.1.1.tar.bz2      
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/libgail-gnome-1.1.1.tar.gz        
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/libgtkhtml-2.6.3.tar.bz2          
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/libgtkhtml-2.6.3.tar.gz         
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/libgtop-2.10.2.tar.bz2            
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/libgtop-2.10.2.tar.gz            
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/librsvg-2.9.5.tar.bz2              
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/librsvg-2.9.5.tar.gz               
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/libsoup-2.2.3.tar.bz2            
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/libsoup-2.2.3.tar.gz            
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/libwnck-2.10.2.tar.bz2            
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/libwnck-2.10.2.tar.gz             
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/libxklavier-2.0.tar.bz2           
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/libxklavier-2.0.tar.gz            
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/metacity-2.10.2.tar.bz2            
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/metacity-2.10.2.tar.gz            
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/nautilus-2.10.1.tar.bz2           
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/nautilus-2.10.1.tar.gz             
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/nautilus-cd-burner-2.10.2.tar.bz2  
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/nautilus-cd-burner-2.10.2.tar.gz    
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/scrollkeeper-0.3.14.tar.bz2        
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/scrollkeeper-0.3.14.tar.gz         
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/startup-notification-0.8.tar.bz2    
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/startup-notification-0.8.tar.gz     
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/system-tools-backends-1.2.0.tar.bz2
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/system-tools-backends-1.2.0.tar.gz  
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/totem-1.0.4.tar.bz2                
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/totem-1.0.4.tar.gz                 
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/vino-2.10.0.tar.bz2                 
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/vino-2.10.0.tar.gz                 
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/vte-0.11.13.tar.bz2               
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/vte-0.11.13.tar.gz                 
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/ximian-connector-2.2.3.tar.bz2     
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/ximian-connector-2.2.3.tar.gz       
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/yelp-2.10.0.tar.bz2               
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/yelp-2.10.0.tar.gz                 
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/zenity-2.10.1.tar.bz2              
wget http://ftp.gnome.org/pub/GNOME/desktop/2.10/2.10.2/sources/zenity-2.10.1.tar.gz                
