#! /bin/bash

# This is for ubuntu xfce bonic 64bit !!!

SD=$PWD

function Checkid {
	Script=$0
	who=$(whoami)
	[ $who == root ] || {
	echo -e "\033[33m you should run it as root. \n usage: sudo $0\n \033[0m" 
	exit 1
	}
}

function Replace_sources.list {
	echo -e "\033[33m First, we should reconfigure this ubuntu-bonic sourcelist\n \033[0m"
	cat /etc/apt/sources.list >> /etc/apt/sources.list.bak 2>/dev/null 
	echo -e "\033[33m Which source.list do your want? tuna or ustc? default tuna \033[0m"
	read apt 
	[ "$apt" == "ustc" ] && cp $SD/confs/ustc.sources.list /etc/apt/sources.list || cp $SD/confs/tuna.sources.list /etc/apt/sources.list
}

function Checknet {
	echo -e "\033[33m Checking your net connection\n \033[0m"
	mkdir cknet 2>/dev/null
	cd cknet
	wget http://mirrors.tuna.tsinghua.edu.cn/ubuntu  1>/dev/null 2>/dev/null || {
	echo -e "\033[33m You may not connected\n \033[0m"
	exit 1
	}
	cd ..
	rm -r cknet 
}

function Removing {
	echo -e "\033[33m We'll purge thunderbird libreoffice and yelp, which you may not use.\n \033[0m"
	apt purge thunderbird* libreoffice* yelp* -y
}

function Install_software {

	apt update
	apt upgrade -y
	echo -e "\033[33m We'll install some basic packages to your computer\n \033[0m"
	apt install screenfetch vim gcc g++ neofetch plank fcitx-googlepinyin xfce4-appmenu-plugin -y
	
	echo -e "\033[33m Installing adobe-flashplugin\n \033[0m" 
	mkidr /usr/lib/adobe-flashplugin/
	tar -xzvf $SD/packages/flash* -C /usr/lib/adobe-flashplugin/	
	
	echo -e "\033[33m Installing vlc and netease-cloud-music\n \033[0m"
	apt install vlc -y
	dpkg -i $SD/packages/netease*
	apt -f install 
	
	echo -e "\033[33m Installing wps-office\n \033[0m"
	dpkg -i $SD/packages/wps*
	
	
}


function Reconfigures {

	echo -e "\033[33m A simple xfpanel-layouts, you may want to use it with panel-setting\n \033[0m"
	cp $SD/themes/panel.tar.bz2 /usr/share/xfpanel-switch/layouts/
	
	echo -e "\033[33m Copying wallpaper and avatar into /usr/share/backgrounds\n \033[0m"
	cp $SD/themes/bz.jpg /usr/share/backgrounds/
	cp $SD/themes/tx.jpg /usr/share/backgrounds/
	
	echo -e "\033[33m Installing fonts\n \033[0m"
	mkdir /usr/share/fonts/mfonts 2>/dev/null
	cp $SD/fonts/*.ttf /usr/share/fonts/mfonts 
	cd /usr/share/fonts/mfonts
	mkfontscale 
	mkfontdir
	cd $SD
	
	echo -e "\033[33m Installing themes\n \033[0m"
	dpkg -i $SD/themes/papirus*
	tar -xJf $SD/themes/icon* 	-C /usr/share/icons
	tar -xJf $SD/themes/cursor* -C /usr/share/icons 
	tar -xJf $SD/themes/theme* 	-C /usr/share/themes
	tar -xJf $SD/themes/plank*	-C /usr/share/plank/themes/Default/
	
	echo -e "\033[33m Now you can configurate your plank\n \033[0m"
	plank --preferences 
}	
	

function Server-setting {
	apt install openssh-server vsftpd tightvncserver
	
	echo -e "\033[33m Setting ftp-server \033[0m"
	cat /etc/vsftpd.conf >> /etc/vsftpd.conf.bak 2>/dev/null
	cp $SD/confs/vsftpd.conf /etc/vsftpd.conf
	
	echo -e "\033[33m Setting vnc-server \033[0m"
	mkdir $HOME/.vnc 2>/dev/null
	touch $HOME/.vnc/xstartup 2>/dev/null
	cp $SD/confs/xstartup $HOME/.vnc/xstartup
	chmod 777 $HOME/.vnc/xstartup
}

function Readme {
	echo " This is written by mzero."
	echo " There's also a QQ in the file 'packages', you can use it!"
	echo " Now you should do your own work to fully configure your desktop."
	echo " Let's start with your clicking the menu, find 'setting'." 
	echo " And then, you'll find a wonderland!"
	
}

Checkid
echo -e "\033[32m \n Let's configure your xubuntu 1804!\n \033[0m"
Replace_sources.list
Checknet
Removing
Install_software
Reconfigures
Server-setting
Readme
exit 0