net-setup eth0
/etc/init.d/sshd start
passwd

fdisk /dev/sda
mke2fs /dev/sda1
mke2fs -j /dev/sda3
mkswap /dev/sda2 && swapon /dev/sda2
mount /dev/sda3 /mnt/gentoo
mkdir /mnt/gentoo/boot
mount /dev/sda1 /mnt/gentoo/boot
cd /mnt/gentoo

links mirrors.163.com
tar xvjpf port* -C /mnt/gentoo/usr
MAKEOPTS="-j3"
mirrorselect -i -o >> /mnt/gentoo/etc/make.conf
mirrorselect -i -r -o >> /mnt/gentoo/etc/make.conf
cp -L /etc/resolv.conf /mnt/gentoo/etc

mount -t proc none /mnt/gentoo/proc
mount -o bind /dev /mnt/gentoo/dev
chroot /mnt/gentoo /bin/bash
env-update && source /etc/profile

emerge --sync
eselect profile list
eselect profile set 2
nano -w /etc/make.conf
USE="gtk gnome qt4 kde dvd alsa hal cdr dbus -ipv6"
nano -w /etc/locale.gen
locale-gen
cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

emerge gentoo-sources
emerge genkernel   
zcat /proc/config.gz > /usr/share/genkernel/arch/x86/kernel-config
genkernel --menuconfig all   
nano -w /etc/fstab
nano -w /etc/conf.d/hostname
nano -w /etc/conf.d/net
config_eth0=("dhcp")
rc-update add net.eth0 default
passwd
emerge syslog-ng vixie-cron
rc-update add syslog-ng default
rc-update add vixie-cron default
emerge reiserfsprogs
emerge dhcpcd
emerge grub
nano -w /boot/grub/grub.conf
grep -v rootfs /proc/mounts > /etc/mtab
grub-install --no-floppy /dev/sda
exit
cd
umount /mnt/gentoo/boot /mnt/gentoo/dev /mnt/gentoo/proc /mnt/gentoo
reboot

emerge ccache
FEATURES="ccache sandbox parallel-fetch"
CCACHE_DIR="/var/tmp/ccache"
CCACHE_SIZE="2G"
emerge xf86-input-evdev

USE="dri nptl xorg sdl"  
INPUT_DEVICES="keyboard mouse"
VIDEO_CARDS="vesa radeon"
emerge xorg-server
env-update && source /etc/profile
Xorg -configure
cp xorg.conf.new /etc/X11/xorg.conf

USE="dri nptl xorg sdl -qt4 -kde X dbus gtk gnome hal -ipv6"
emerge gnome-light
emerge droid
emerge wqy-bitmapfont
LINGUAS="zh_CN zh"
ACCEPT_KEYWORDS="x86"
emerge gnome-menus
emerge gnome-control-center   调音量工具
emerge gnome-applets   监视器部分工具
emerge firefox 
USE="xft" emerge fcitx
emerge networkmanager   有线无线网络连接工具(本地连接)
emerge audacious 听歌
#emerge alse-utils
#alsamixer
emerge gentoolkit
emerge eix
emerge git
emerge kpdf pdf阅读
emerge amule下载
emerge gedit
emerge gthumb图片
emerge openoffice-bin
USE="qq gtk" emerge -av pidgin